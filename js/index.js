const canvas = document.getElementById('canvas'); 
canvas.width = 600;
canvas.height = window.innerHeight - 100; 

const ctx = canvas.getContext("2d"); 

let cubsArr = [];
let started = false;
const height = 35;
const minColums = 3;
const maxColums = 8;

const createBetweenNumbers = (min, max) => Math.floor(Math.random() * ((max - min) + 1) + min);
const createRandomColor = (r, g, b) => `rgb(${ Math.floor(Math.random() * r) }, ${ Math.floor(Math.random() * g) }, ${ Math.floor(Math.random() * b) })`;

const dataTab = {
    width: 150,
    height: 5,
    x: canvas.width / 2 - 75,
    y: canvas.height - 50,
    dx: 2,
    right: false,
    left: false,
}

const dataBall = {
    size: 12,
    x: canvas.width / 2,
    y: canvas.height - 62,
    dx: 2, 
    dy: -2,
}

const createCubs = () => {
    for (let i = 0; i < createBetweenNumbers(minColums, maxColums); i++) {
        let width = canvas.width / createBetweenNumbers(minColums, maxColums);
        for (let j = 0; j < canvas.width/width; j++) {
            let color = `${createRandomColor(230, 130, 180)}`;
            cubsArr.push(
                {
                    x: j * width,
                    y: i * height,
                    width,
                    height,
                    color,
                }
            );
        }
    }
}

const drawCubs = () => {
    cubsArr.forEach(cub => {
        ctx.fillStyle = cub.color;
        ctx.fillRect(
            cub.x, 
            cub.y, 
            cub.width, 
            cub.height,
        ); 
    });
}

const drawTab = () => {
    ctx.beginPath();
    ctx.fillStyle = 'red';
    ctx.fillRect(
        dataTab.x, 
        dataTab.y, 
        dataTab.width, 
        dataTab.height
    );
    ctx.closePath();
}

const tabMove = e => {
    if (e.keyCode === 39) {
        dataTab.right = true;
    }
    
    if (e.keyCode === 37) {
        dataTab.left = true;
    }
}

const tabStop = e => {
    if (e.keyCode === 39) {
        dataTab.right = false;
    }
    
    if (e.keyCode === 37) {
        dataTab.left = false;
    }
}

document.addEventListener("keydown", tabMove);
document.addEventListener("keyup", tabStop);

const changeTabPosition = () => {
    if (dataTab.right) {
        dataTab.x += dataTab.dx;
        if (dataTab.x + dataTab.width > canvas.width) {
            dataTab.x = canvas.width - dataTab.width;
        }
    }
    
    if (dataTab.left) {
        dataTab.x -= dataTab.dx;
        if (dataTab.x < 0) {
            dataTab.x = 0;
        }
    }
}

const drawBall = () => {
    size = dataBall.size;
    ctx.beginPath();
    ctx.arc(
        dataBall.x, 
        dataBall.y, 
        dataBall.size, 
        0, 
        2 * Math.PI,
    );
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

const changeBallPosition = () => {
    
    dataBall.x += dataBall.dx;
    dataBall.y += dataBall.dy;

    if (dataBall.x >= dataTab.x && dataBall.x <= (dataTab.x + dataTab.width)) {
        if (dataBall.y + dataBall.size > dataTab.y) {
            dataBall.dy = -dataBall.dy;
        }

        if (dataBall.y + dataBall.size / 2 >= dataTab.y) {
            dataBall.dx = -dataBall.dx;
            dataBall.dy = -dataBall.dy;
        }
    }

    if (dataBall.x + dataBall.size >= canvas.width) {
        dataBall.x = canvas.width - dataBall.size;
        dataBall.dx = -dataBall.dx;
    }

    if (dataBall.y <= 0) {
        dataBall.y = 0;
        dataBall.dy = -dataBall.dy;
    }
    
    if (dataBall.x - dataBall.size <= 0 ) {
        dataBall.x = dataBall.size;
        dataBall.dx = -dataBall.dx;
    }
}

const newGame = interval => {
    clearInterval(interval);
    ctx.clearRect(
        0, 
        0, 
        canvas.width, 
        canvas.height,
    );
    cubsArr = [];
    createCubs();
    dataBall.x = canvas.width / 2;
    dataBall.y = canvas.height - 62;
    dataBall.dx = 2;
    dataBall.dy = -2;
    dataTab.x = canvas.width / 2 - 75;
    dataTab.dx = 2;
    dataTab.left = false;
    dataTab.right = false;
    started = false;
    draw();
}

const breakeCubs = () => {
    cubsArr.forEach(cub => {
        if (dataBall.y  <= cub.y + cub.height) {
            if (dataBall.x >= cub.x && dataBall.x <= cub.x + cub.width) {
                dataBall.dy = -dataBall.dy;
                let indexOfDeletedCub  = cubsArr.indexOf(cub);
                cubsArr.splice(indexOfDeletedCub,1);
                ctx.clearRect(
                    0, 
                    0, 
                    canvas.width, 
                    canvas.height,
                );
                drawCubs();
            }
        }
    });
}

const win = () => {
    if (cubsArr.length === 0) {
        alert('You Win');
        newGame(interval);
    }
}

const looseGame = interval => {
    if (dataBall.y >= canvas.height) {
        alert('You Loose');
        newGame(interval);
    }
}

let interval;
const game = () => {
    win();
    looseGame(interval);
    
    ctx.clearRect(
        0, 
        0, 
        canvas.width, 
        canvas.height,
    );

    changeTabPosition();
    changeBallPosition();
    
    breakeCubs();
    draw();
}

const start = () => {
    if (started) {
        return;
    }
    started = true;
    interval = setInterval(game, 1);
}

const draw = () => {
    drawCubs();
    drawTab();
    drawBall();
}

createCubs();
draw();

document.addEventListener('keydown', e => {
   if (e.keyCode === 32) {
       start();
   }
});